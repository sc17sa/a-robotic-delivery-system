## Requirements:
	 - 4GB minimum RAM (I recommend at least 12GB) 
	 - Ubuntu 18.04 LTS
	 - ROS Melodic Morenia

## Installation of Ubuntu with ROS Melodic Morenia and TIAGo simulation:

    Complete the steps as shown in:
        http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/InstallUbuntuAndROS
    Complete the first step as shown in
        http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation
    
    Copy the files from GitLab to the src directory
    
    Skip the second step (rosinstall)
    
    Continue with setting up rosdep
    
## Build the packages:

    Run in ~/tiago_public_ws/:
	 - source /opt/ros/melodic/setup.bash
	 - catkin build -DCATKIN_ENABLE_TESTING=0
	 - source ./devel/setup.bash
        
	Copy the custom.world file from src/tiago_pick_demo/worlds/ to ~/.gazebo/worlds/

## Execution

    Open 3 terminal windows in tiago_public_ws directory
    
    Execute the following commands in order
	 - roslaunch tiago_pick_demo pick_simulation.launch
	 - roslaunch tiago_pick_demo pick_demo.launch
	 - rosservice call /pick_gui
